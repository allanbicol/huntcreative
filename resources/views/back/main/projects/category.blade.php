@extends('back.admin-template')

@section('breadcrumb')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between align-items-center">
            <div>
                <h4 class="font-weight-bold mb-0">Project Categories</h4>
            </div>

        </div>
    </div>
</div>
@endsection


@section('content')
<form method="POST" action="{{ route('projects-add-category')}}">
        {{ csrf_field() }}
<div class="row">

    <div class="col-md-5 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h5>Add New Category</h5>
                @if($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <strong>{{ $message }}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </u>

                    </div>
                @endif
                <div class="form-group">
                <label for="proj-name">Name</label>
                <input type="text" name="name" class="form-control" id="proj-name" autocomplete="off" placeholder="">
                <small><i>The name is how it appears on your site</i></small>
                </div>

                <div class="form-group">
                <label for="proj-slug">Slug</label>
                <input type="text" name="slug" class="form-control" id="proj-slug" placeholder="">
                <small><i>The "slug" is the URL-friendly version of the name. It is ussually lowercase and contains only letters, numbers and hyphens.</i></small>
                </div>
                <div class="form-group">
                <label for="exampleInputConfirmPassword1">Description</label>
                <textarea class="form-control" name="description" rows="5"></textarea>
                </div>
                <button type="submit" class="btn btn-rounded btn-block btn-primary mr-2">Add New Category</button>
            </div>
        </div>
    </div>
    <div class="col-md-7 grid-margin stretch-card">
        <div class="card">
                <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                    <thead>
                        <tr>
                        <th>
                            Name
                        </th>
                        <th>
                            Description
                        </th>
                        <th>
                            Slug
                        </th>
                        <th>
                            Action
                        </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($categories as $category)
                            <tr>
                                <td>{{ $category->name }}</td>
                                <td>{{ $category->description }}</td>
                                <td>{{ $category->slug }}</td>

                                <td>
                                    <button type="button" class="btn btn-primary btn-rounded btn-icon">
                                        <i class="ti-pencil"></i>
                                    </button>
                                    <button type="button" class="btn btn-danger btn-rounded btn-icon">
                                        <i class="ti-trash"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach

                    </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>

</div>
   </form>
@endsection

@section('script')
    <script>

        $("#proj-name").on('keyup',function(){
            strName = $("#proj-name").val();
            str = strName.replace(/\s+/g, '-').toLowerCase();
            console.log(str);
            $("#proj-slug").val(str);
        });
    </script>
@endsection

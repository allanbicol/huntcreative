@extends('back.admin-template')

@section('breadcrumb')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between align-items-center">
        <div>
            <h4 class="font-weight-bold mb-0">Projects</h4>
        </div>
        <div>
        <a class="btn btn-primary btn-icon-text btn-rounded" href="{{ route('projects-add') }}">
                <i class="ti-plus btn-icon-prepend"></i>Add New
            </a>
        </div>
        </div>
    </div>
</div>
@endsection


@section('content')
<div class="col-lg-12 grid-margin stretch-card">
    <div class="card">
        <div class="card-body">
        <h4 class="card-title">List of projects</h4>

        <div class="table-responsive">
            <table class="table table-striped">
            <thead>
                <tr>
                <th>
                    No
                </th>
                <th>
                    Project Name
                </th>
                <th>
                    Client
                </th>
                <th>
                    Website
                </th>
                <th>
                Category
                </th>
                <th>
                    Action
                </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($projects as $project)
                    <tr>
                        <td>{{ $project->id }}</td>
                        <td>{{ $project->name }}</td>
                        <td>{{ $project->client_name }}</td>
                        <td><a href="{{ $project->website }}">{{ $project->website }}</a></td>
                        <td>{{ $project->category }}</td>
                        <td>
                            <button type="button" class="btn btn-primary btn-rounded btn-icon">
                                <i class="ti-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-danger btn-rounded btn-icon">
                                <i class="ti-trash"></i>
                            </button>
                        </td>
                    </tr>
                @endforeach

            </tbody>
            </table>
        </div>
        </div>
    </div>
</div>
@endsection

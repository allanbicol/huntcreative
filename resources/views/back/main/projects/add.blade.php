@extends('back.admin-template')

@section('breadcrumb')
<div class="row">
    <div class="col-md-12 grid-margin">
        <div class="d-flex justify-content-between align-items-center">
            <div>
                <h4 class="font-weight-bold mb-0">Add Project</h4>
            </div>

        </div>
    </div>
</div>
@endsection


@section('content')
<form method="POST" action="{{ route('projects-add-action')}}">
        {{ csrf_field() }}
<div class="row">

    <div class="col-md-9 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                <label for="exampleInputUsername1">Project Name</label>
                <input type="text" name="name" class="form-control" id="projectname" autocomplete="off" placeholder="">
                </div>
                <div class="form-group">
                <label for="exampleInputEmail1">Client</label>
                <input type="text" name="client_name" class="form-control" autocomplete="off" id="exampleInputEmail1" placeholder="">
                </div>
                <div class="form-group">
                <label for="exampleInputPassword1">Website</label>
                <input type="text" name="website" class="form-control" autocomplete="off" id="exampleInputPassword1" placeholder="">
                </div>
                <div class="form-group">
                <label for="exampleInputConfirmPassword1">Projects Details</label>
                <textarea name="details"></textarea>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-3 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <button type="submit" class="btn btn-rounded btn-block btn-primary mr-2">Publish</button>
                    <br>
                    <h4>Select Category</h4>
                    <div class="form-group">
                        @foreach ($categories as $category)
                        <div class="form-check">
                                <label class="form-check-label">
                                <input name="category[]" type="checkbox" value="{{ $category->slug }}" class="form-check-input">
                                {{ $category->name}}
                                </label>
                            </div>
                        @endforeach


                    </div>



                </div>
            </div>
        </div>

</div>
   </form>
@endsection

@section('script')
    <script>
            CKEDITOR.replace( 'details' );


    </script>
@endsection

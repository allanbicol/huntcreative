<!DOCTYPE html>
<html lang="en">
<head>
    @yield('meta-title')
    <meta charset="UTF-8">
    @yield('meta')
    <meta name="description" content="Just a bunch of creative creatures creating something and everything.">
	<meta name="keywords" content="hunt, create, design, write, develop">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ URL::asset('front-theme/images/icon.png')}}">
	<!-- Stylesheets -->
	<link rel="stylesheet" href="{{ URL::asset('front-theme/css/bootstrap.min.css')}}"/>
    <link rel="stylesheet" href="{{ URL::asset('front-theme/cubeportfolio/css/cubeportfolio.min.css')}}"/>
    <link rel="stylesheet" href="{{ URL::asset('front-theme/css/owl.theme.css')}}"/>
    <link rel="stylesheet" href="{{ URL::asset('front-theme/css/owl.carousel.css')}}"/>
    <link rel="stylesheet" href="{{ URL::asset('front-theme/css/colors/red.css')}}"/>
    <link rel="stylesheet" href="{{ URL::asset('front-theme/css/style.css')}}"/>
    <link rel="stylesheet" href="{{ URL::asset('front-theme/css/animate.css')}}"/>

    <!-- Google Web fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800" rel="stylesheet">

    <!-- Font icons -->
    <link rel="stylesheet" href="{{ URL::asset('front-theme/icon-fonts/fontawesome-5.0.6/css/fontawesome-all.min.css')}}"/>
    <link rel="stylesheet" href="{{ URL::asset('front-theme/icon-fonts/flaticon/flaticon.css')}}" />

</head>
<body>

    <!-- PRELOADER -->
    {{-- <div class="preloader">
        <div class="loader">
            <div class="loader-inner"></div>
        </div>
    </div> --}}

    <!-- HEADER -->
    <header>
    <h2 class="logo"><a href="{{ route('homepage') }}"><img src="{{ URL::asset('front-theme/images/icon.png')}}" width="50px" alt=""><span>hunt creative</span></a></h2>
        <div class="nav-icon" >
            <span></span>
            <span></span>
            <span></span>
        </div>
    </header>

    <!-- FULL MENU -->
    <div class="full-menu">
        <div class="full-inner row">
            <nav class="col-md-8">
                <ul>
                <li><a href="{{ route('homepage') }}">home</a></li>
                    <li><a href="{{ route('aboutus') }}">about</a></li>
                    <li><a href="{{ route('portfolio') }}">portfolio</a></li>
                    <li><a href="{{ route('services') }}">services</a></li>
                    <li><a href="#">blog</a></li>
                    <li><a href="{{ route('contact') }}">connect with us</a></li>
                </ul>
            </nav>
            <div class="col-md-4 full-contact">
                <ul>
                    <li class="title">Get in Touch</li>
                    <li style="color:#4e4e4e;">helene@huntcreative.co.nz</li>
                    <li>
                        <div class="social">
                                <a href="https://www.facebook.com/HuntCreativeLtd/?ref=bookmarks"><i class="fab fa-facebook" aria-hidden="true"></i>  </a>
                                <a href="https://www.instagram.com/huntcreativeltd/"><i class="fab fa-instagram" aria-hidden="true"></i>  </a>
                                <a href="https://www.linkedin.com/in/h%C3%A9l%C3%A8ne-hunt-42786645/"><i class="fab fa-linkedin" aria-hidden="true"></i>  </a>
                                <a href="https://www.pinterest.ph/HuntCreativeLtd/ "><i class="fab fa-pinterest" aria-hidden="true"></i>  </a>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- SITE CONTENT -->
    <div class="wrapper">
        @yield('title')

        @yield('content')


    </div> <!-- wrapper end -->

    <footer>
            <div class="cont">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12 copyright">
                            <img class="lazy" data-src="{{ URL::asset('front-theme/images/icon.png')}}" width="70px" alt="">
                            <p>© 2019 hunt creative</p>
                        </div>
                        <div class="col-md-4 d-sm-none d-md-block">
                            <div class="social" style="font-size:20px;">
                                    <a href="https://www.facebook.com/HuntCreativeLtd/?ref=bookmarks"><i class="fab fa-facebook" aria-hidden="true"></i>  </a>
                                    <a href="https://www.instagram.com/huntcreativeltd/"><i class="fab fa-instagram" aria-hidden="true"></i>  </a>
                                    <a href="https://www.linkedin.com/in/h%C3%A9l%C3%A8ne-hunt-42786645/"><i class="fab fa-linkedin" aria-hidden="true"></i>  </a>
                                    <a href="https://www.pinterest.ph/HuntCreativeLtd/"><i class="fab fa-pinterest" aria-hidden="true"></i>  </a>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12 d-none d-sm-block getintouch">
                            <a href="#">
                                <strong>Get In Touch</strong><br>
                                <p>helene@huntcreative.co.nz</p>
                            </a>
                        </div>
                    </div>
               </div>
    </footer>
    <div style="position:fixed;bottom:60px;left:0px;">
        <a href="#" onclick="window.open('https://www.sitelock.com/verify.php?site=huntcreative.co.nz','SiteLock','width=600,height=600,left=160,top=170');" ><img class="img-responsive" alt="SiteLock" title="SiteLock" src="//shield.sitelock.com/shield/huntcreative.co.nz" /></a>

    </div>

    <!-- Javascripts -->
    {{-- <script src="{{ URL::asset('front-theme/js/particles.js')}}"></script> --}}
    <script src="{{ URL::asset('front-theme/js/jquery-2.1.4.min.js')}}"></script>
    <script src="{{ URL::asset('front-theme/cubeportfolio/js/jquery.cubeportfolio.min.js')}}"></script>
    <script src="{{ URL::asset('front-theme/js/typed.js')}}"></script>

    {{-- <script src="{{ URL::asset('front-theme/js/app.js')}}"></script> --}}
    <script src="{{ URL::asset('front-theme/js/jquery.hover3d.js')}}"></script>
    <script src="{{ URL::asset('front-theme/js/owl.carousel.min.js')}}"></script>
    <script src="{{ URL::asset('front-theme/js/main.js')}}"></script>
    <script src="{{ URL::asset('front-theme/js/wow.js')}}"></script>
    <script src="{{ URL::asset('front-theme/js/yall.min.js')}}"></script>
    <script>
        var wow = new WOW(
        {
            boxClass:     'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset:       0,          // distance to the element when triggering the animation (default is 0)
            mobile:       true,       // trigger animations on mobile devices (default is true)
            live:         true,       // act on asynchronously loaded content (default is true)
            callback:     function(box) {
            // the callback is fired every time an animation is started
            // the argument that is passed in is the DOM node being animated
            },
            scrollContainer: null // optional scroll container selector, otherwise use window
        }
        );
        wow.init();

        document.addEventListener("DOMContentLoaded", yall);
    </script>
    <script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: '942022f4-b5d7-48a3-82f3-7330a0f0e552', f: true }); done = true; } }; })();</script>
    @yield('script')
</body>
</html>

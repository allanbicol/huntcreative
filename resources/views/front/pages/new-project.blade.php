@extends('front.page-template')
@section('title')
<title>We are hunt creative - Portfolio</title>
@endsection

@section('title')
    <section class="titlebar">
        <h1 class="page-title">start a <span>project</span></h1>
        <div id="particles-js"></div>
    </section>

    <hr class="col-md-6 bottom_60">
@endsection

@section('content')
<div class="cont">

    <section id="graphic-design" class="services">
        <div class="row">

            <div class="col-md-12" >

                <div class="row" style="margin-top:40px;">


                    <div class="col-md-12">
                        <div class="new-project">
                        <p>Hey there,</p>
                        <br>
                        <p>My name is <input style="width:40%;" type="text" class="input" name="fullname" placeholder="first and last name" required/> and I want to start a new project.</p>
                        <p>I work for <input style="width:70%;" type="text" class="input" name="company" placeholder="company name"/> and</p>
                        <p>I could use your service for
                            <ul class="ks-cboxtags">
                                <li><input type="checkbox" name="service[]" id="checkboxOne" value="Graphic Design"><label for="checkboxOne">Graphic Design</label></li>
                                <li><input type="checkbox" name="service[]" id="checkboxTwo" value="Content Writing" ><label for="checkboxTwo">Content Writing</label></li>
                                <li><input type="checkbox" name="service[]" id="checkboxThree" value="Web Development" ><label for="checkboxThree">Web Development</label></li>
                            </ul>
                        </p>
                        <br>
                        <p>You can reach me at <input style="width:60%;" type="text" class="input" name="phone" placeholder="phone number" required/></p>
                        <p>or get in touch by email at <input style="width:55%;" type="email" class="input" name="email" placeholder=" email address" autocomplete="off" required/>.</p>
                        </div>
                    </div>
                    <div class="col-md-12 text-center" style="margin-top:100px;"> <a href="#" class="btn btn-white btn-animation-1 wow animated bounceInUp">Get in Touch</a></div>
                </div>

            </div>

        </div>
    </section>




</div> <!-- cont end -->
@endsection

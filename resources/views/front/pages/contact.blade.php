@extends('front.page-template')
@section('meta-title')
<title>We are hunt creative - Connect with us</title>
@endsection
@section('title')
    <section class="titlebar">
        <h1 class="page-title">let's talk <span>creativity</span></h1>
        {{-- <div id="particles-js"></div> --}}
    </section>

    <hr class="col-md-6 bottom_60">
@endsection

@section('content')
<div class="cont">
    <section class="contact col-md-8 offset-md-2 top_90">
        <div class="contact-info text-center wow animated fadeInUp">
            @if($message = Session::get('success'))
                <div class="col-md-12 text-center wow animated fadeInUp">
                    <div class="alert alert-success alert-block">
                        <strong>{{ $message }}</strong>
                    </div>
                </div>
            @endif
            <br>
            <p>Your Brand - Your Business - Your Products - Your Services
                <br>
                Hunt Creative is ready to provide the creative and digital solutions you're looking for.
            </p>
        </div>

        <div class="contact-info text-center wow animated fadeInUp" style="padding-top:100px;">
            <!-- <p>1444 S. Alameda Street Los Angeles, California 90021 </p>  -->
            <a href="mailto:studio@huntcreative.co.nz">helene@huntcreative.co.nz</a>
            <p>+64 21 393 331</p>
        </div>
        <div class="contact-form top_90">
        <form class="row" method="POST" action="{{ route('send-mail') }}">
            {{ csrf_field() }}
                <div class="col-md-6 wow animated fadeInLeft">
                    <input class="inp" name="name" type="text" placeholder="Full Name" autocomplete="off" required>
                </div>
                <div class="col-md-6 wow animated fadeInRight">
                    <input class="inp" name="company" type="text" placeholder="Company" autocomplete="off" required>
                </div>
                <div class="col-md-6 wow animated fadeInRight">
                    <input class="inp" name="email" type="email" placeholder="Email" autocomplete="off" required>
                </div>
                <div class="col-md-6 wow animated fadeInLeft">
                    <input class="inp" name="phone" type="text" placeholder="Phone" autocomplete="off" required>
                </div>
                <div class="col-md-12 wow animated fadeInLeft">
                    <h5>Interested in</h5>
                    <ul class="ks-cboxtags">
                        <li><input type="checkbox" name="service[]" id="checkboxOne" value="Graphic Design"><label for="checkboxOne">Graphic Design</label></li>
                        <li><input type="checkbox" name="service[]" id="checkboxTwo" value="Content Writing" ><label for="checkboxTwo">Content Writing</label></li>
                        <li><input type="checkbox" name="service[]" id="checkboxThree" value="Web Development" ><label for="checkboxThree">Web Development</label></li>
                        <li><input type="checkbox" name="service[]" id="checkboxFour" value="Social Media" ><label for="checkboxFour">Social Media</label></li>
                    </ul>
                </div>
                <div class="col-md-12 wow animated fadeInUp">
                    <textarea placeholder="Details and/or clarifications" name="message" rows="6" class="col-md-12 form-message"></textarea>
                </div>
                <div class="col-md-12 text-center wow animated fadeInUp">
                    <input type="submit" value="Submit" class="site-btn2">
                </div>


            </form>
        </div>



    </section>

</div> <!-- cont end -->
@endsection

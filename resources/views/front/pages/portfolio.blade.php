@extends('front.page-template')
@section('meta-title')
<title>We are hunt creative - Portfolio</title>
@endsection

@section('title')
    <section class="titlebar">
        <h1 class="page-title"><span>our </span>bestfriends</h1>
        {{-- <div id="particles-js"></div> --}}
    </section>

    <hr class="col-md-6 bottom_60">
@endsection

@section('content')
<div class="cont">

    <section class="about">
        <!-- ABOUT TEXT -->
        <div class="about-text text-center top_90" style="margin-bottom:100px;">

            <h2 class="subtitle">THE CREATIVE SIDE OF EVERYTHING AND EVERYONE</h2><br><br>
            <h5 style="line-height: 36px;color: #6f6f6f;">Meet our Bffs who gave us the opportunity to work with them with their branding and positioning (mostly from scratch). We also gave them a hand with their websites and keep this one on the down low, we even helped one of our client’s nephew with his art project, he got an A+ (we think). Nothing but love and creativity for you all.</h5>
        </div>

    </section>

    <section id="portfolio" class="portfolio" >

            {{-- <div class="portfolio-filter row">
                <div data-filter=".digital" class="cbp-filter-item">Digital Works</div>
                <div data-filter=".photography" class="cbp-filter-item">Photography</div>
                <div data-filter=".branding" class="cbp-filter-item">Branding</div>
                <div data-filter="*" class="cbp-filter-item cbp-filter-item-active">All Works</div>
            </div> --}}
            <div id="grid-container">
                <!-- Item -->
                <div class="cbp-item photography wow animated bounceInUp">
                    <a href="{{ route('project1') }}">
                        <figure class="fig">
                            <img src="{{ URL::asset('front-theme/images/portfolio/work-1/logo.jpg')}}" alt="">
                            <figcaption>
                                <h3>Offsure Global</h3>
                                <p>graphic design, content, web development</p>
                            </figcaption>
                        </figure>
                    </a>
                </div>
                <!-- Item -->
                <div class="cbp-item digital branding wow animated bounceInDown">
                    <a href="{{ route('project2') }}">
                        <figure class="fig">
                            <img src="{{ URL::asset('front-theme/images/portfolio/work-2/logo.jpg')}}" alt="">
                            <figcaption>
                                <h3>Cocoon House</h3>
                                <p>graphic design, content, web development</p>
                            </figcaption>
                        </figure>
                    </a>
                </div>
                <!-- Item -->
                <div class="cbp-item digital wow animated bounceInRight">
                    <a href="#">
                        <figure class="fig">
                            <img src="{{ URL::asset('front-theme/images/portfolio/work-3/logo.jpg')}}" alt="">
                            <figcaption>
                                <h3>Proper Pizza </h3>
                                <p>web development</p>
                            </figcaption>
                        </figure>
                    </a>
                </div>

                <div class="cbp-item digital wow animated bounceInLeft">
                    <a href="#">
                        <figure class="fig">
                            <img src="{{ URL::asset('front-theme/images/portfolio/work-4/logo.jpg')}}" alt="">
                            <figcaption>
                                <h3>Waikato Valley </h3>
                                <p>graphic design</p>
                            </figcaption>
                        </figure>
                    </a>
                </div>

                <div class="cbp-item digital wow animated bounceInUp">
                    <a href="#">
                        <figure class="fig">
                            <img src="{{ URL::asset('front-theme/images/portfolio/work-5/logo.jpg')}}" alt="">
                            <figcaption>
                                <h3>TUI MOANA </h3>
                                <p>graphic desig, content, web development</p>
                            </figcaption>
                        </figure>
                    </a>
                </div>

            </div>
            <!-- load more button -->
            {{-- <div id="port-loadMore" class="cbp-l-loadMore-button top_120 bottom_90">
                <a href="port.html" class="cbp-l-loadMore-link site-btn" rel="nofollow">
                    <span class="cbp-l-loadMore-defaultText">Load More (<span class="cbp-l-loadMore-loadItems">2</span>)</span>
                    <span class="cbp-l-loadMore-loadingText">Loading...</span>
                    <span class="cbp-l-loadMore-noMoreLoading">No More Works</span>
                </a>
            </div> --}}
    </section>
</div> <!-- cont end -->
@endsection

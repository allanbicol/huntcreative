@extends('front.page-template')
@section('meta-title')
<title>We are hunt creative - About</title>
@endsection

@section('title')
    <section class="titlebar">
        <h1 class="page-title">relentless<span> + </span>creative</h1>
        {{-- <div id="particles-js"></div> --}}
    </section>

    <hr class="col-md-6 bottom_60">
@endsection

@section('content')
<div class="cont">

    <section class="about">
        <!-- ABOUT TEXT -->
        <div class="about-text top_90">
            <p class="wow animated fadeInUp" >From small beginnings in 2008, Hunt Creative emerged as a freelance graphic design resource - now over a decade later, we are a collective of creatives - designing, developing and writing - working together in the name of branding and design solutions.</p>
            <p class="wow animated fadeInUp" style="margin-top:15px;">Although not rocket science, we are obsessed with what we do and remain true to our core values of relentless creativity + meaningful design to help both individuals and businesses build brands, tell stories and share them with the world.</p>
            <p class="wow animated fadeInUp" style="margin-top:15px;">How can we help you make an impact with your business? Let’s talk and make a plan on the where to from here.</p>

            <p class="wow animated fadeInUp" style="margin-top:15px;">We keep it simple, we make it beautiful.<br>
            We are Hunt Creative.<br>
            Let’s talk.</p>
        </div>
        <!-- WORK AREAS -->
        {{-- <div class="owl-carousel work-areas text-center top_120 bottom_90 col-md-10 offset-md-1" data-pagination="true" data-autoplay="3000" data-items-desktop="3" data-items-desktop-small="3" data-items-tablet="2" data-items-tablet-small="1">

            <div class="area col-md-12 item">
                <i class="flaticon-024-computer-graphic"></i>
                <h3 class="title" class="title">Web Design</h3>
                <div class="line"></div>
                <p>His room, a proper human room although a little too small, lay peacefully between its four familiar walls.</p>
            </div>

            <div class="area col-md-12 item">
                <i class="flaticon-007-stationery"></i>
                <h3 class="title">Branding Identity</h3>
                <div class="line"></div>
                <p>His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked.</p>
            </div>

            <div class="area col-md-12 item">
                <i class="flaticon-025-graphic-design"></i>
                <h3 class="title">Illustration</h3>
                <div class="line"></div>
                <p>One morning, when Gregor Samsa woke from troubled dreams, he found transformed in his bed into.</p>
            </div>

            <div class="area col-md-12 item">
                <i class="flaticon-024-computer-graphic"></i>
                <h3 class="title" class="title">Web Design</h3>
                <div class="line"></div>
                <p>His room, a proper human room although a little too small, lay peacefully between its four familiar walls.</p>
            </div>

            <div class="area col-md-12 item">
                <i class="flaticon-007-stationery"></i>
                <h3 class="title">Branding Identity</h3>
                <div class="line"></div>
                <p>His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked.</p>
            </div>

            <div class="area col-md-12 item">
                <i class="flaticon-025-graphic-design"></i>
                <h3 class="title">Illustration</h3>
                <div class="line"></div>
                <p>One morning, when Gregor Samsa woke from troubled dreams, he found transformed in his bed into.</p>
            </div>
        </div> --}}

        <!-- WORK AREAS -->
        {{-- <div class="clients">
            <div class="row">

                <div class="col-md-3 col-sm-6 client">
                    <figure>
                        <img src="images/clients/client-5.jpg">
                    </figure>
                </div>

                <div class="col-md-3 col-sm-6 client">
                    <figure>
                        <img src="images/clients/client-6.jpg">
                    </figure>
                </div>

                <div class="col-md-3 col-sm-6 client">
                    <figure>
                        <img src="images/clients/client-7.jpg">
                    </figure>
                </div>

                <div class="col-md-3 col-sm-6 client">
                    <figure>
                        <img src="images/clients/client-8.jpg">
                    </figure>
                </div>
            </div>
        </div> --}}
    </section>
</div> <!-- cont end -->
@endsection

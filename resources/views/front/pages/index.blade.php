@extends('front.page-template')
@section('meta-title')
<title>We are hunt creative - Home</title>
@endsection
@section('content')

{{-- <section class="home" style="background-image:url({{ URL::asset('front-theme/images/home2.jpg')}});background-size:cover;">
   <div id="particles-js"></div>
    <div class="home-content">
        <p class="hero-title">We <span class="element" data-text1="Create" data-text2="Design" data-text3="Develop" data-text4="Write" data-text5="Interact" data-loop="true" data-backdelay="1500"></span></p>
        <p>relentless creativity + meaningful design</p>
        <div class="social">
            <a class="text">social links</a>
            <div class="line"></div>
            <a href="https://www.facebook.com/HuntCreativeLtd/?ref=bookmarks"><i class="fab fa-facebook" aria-hidden="true"></i>  </a>
            <a href="https://www.instagram.com/huntcreativeltd/"><i class="fab fa-instagram" aria-hidden="true"></i>  </a>
            <a href="https://www.linkedin.com/in/h%C3%A9l%C3%A8ne-hunt-42786645/"><i class="fab fa-linkedin" aria-hidden="true"></i>  </a>
            <a href="https://www.pinterest.ph/HuntCreativeLtd/"><i class="fab fa-pinterest" aria-hidden="true"></i>  </a>
        </div>


    </div>

</section> --}}
<section class="home" style="padding-left:50px;padding-right:50px;">
        <div class="home-content port-header">
            <!-- <h1 class="hero-title">There isn't one plan<br> that fits all</h1> -->
            <p class="hero-title">We <span class="element" data-text1="Create" data-text2="Design" data-text3="Develop" data-text4="Write" data-text5="Interact" data-loop="true" data-backdelay="1500"></span></p>
            <p>relentless creativity + meaningful design</p>



        </div>

        <div class="row portfolio-tile">
                <div class="col-lg-3"></div>
                <div class="col-lg-9">
                        <div class="rows">
                            <div class="column">

                            </div>
                            <div class="column">
                            </div>
                            <div class="column">
                                <a href="#">
                                <div class="flip-container col3" ontouchstart="this.classList.toggle('hover');">
                                    <div class="flipper">
                                        <div class="front">
                                                <img  src="{{ URL::asset('front-theme/images/clients/img1.jpg')}}" width="100%">
                                        </div>
                                        <div class="back" style="background-color:#8a7747">
                                            <img  src="{{ URL::asset('front-theme/images/clients/virtue.png')}}" width="60%">
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                            <div class="column">
                                <div class="flip-container col4" ontouchstart="this.classList.toggle('hover');">
                                    <div class="flipper">
                                        <div class="front">
                                                <img  src="{{ URL::asset('front-theme/images/clients/img2.jpg')}}" width="100%">
                                        </div>
                                        <div class="back" style="background-color:#1a9f97;">
                                            <img  src="{{ URL::asset('front-theme/images/clients/odc.png')}}" width="50%">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="rows">
                            <div class="column">
                                    <div class="home-content">
                                <p class="hero-title">We <span class="element" data-text1="Create" data-text2="Design" data-text3="Develop" data-text4="Write" data-text5="Interact" data-loop="true" data-backdelay="1500"></span></p>
                                <p>relentless creativity + meaningful design</p>
                                    </div>
                            </div>
                            <div class="column">
                                    <div class="flip-container col2" ontouchstart="this.classList.toggle('hover');">
                                            <div class="flipper">
                                                <div class="front">
                                                        <img  src="{{ URL::asset('front-theme/images/clients/img3.jpg')}}" width="100%">
                                                </div>
                                                <div class="back" style="background-color:#ffca08;">
                                                    <img  src="{{ URL::asset('front-theme/images/clients/waikato.png')}}" width="50%">
                                                </div>
                                            </div>
                                        </div>
                            </div>
                            <div class="column">
                                    <div class="flip-container col3" ontouchstart="this.classList.toggle('hover');">
                                            <div class="flipper">
                                                <div class="front">
                                                        <img  src="{{ URL::asset('front-theme/images/clients/img4.jpg')}}" width="100%">
                                                </div>
                                                <div class="back" style="background-color:#002d66;">
                                                    <img  src="{{ URL::asset('front-theme/images/clients/ccc.png')}}" width="40%">
                                                </div>
                                            </div>
                                        </div>
                            </div>
                            <div class="column">
                                <a href="{{route('project1')}}">
                                    <div class="flip-container col4" ontouchstart="this.classList.toggle('hover');">
                                            <div class="flipper">
                                                <div class="front" style="">
                                                        <img  src="{{ URL::asset('front-theme/images/clients/img5a.jpg')}}" width="100%" >
                                                </div>
                                                <div class="back" style="background-color:#FF4813;">
                                                        <img  src="{{ URL::asset('front-theme/images/clients/offsure.png')}}" width="35%" >
                                                </div>
                                            </div>
                                        </div>
                                </a>
                            </div>
                        </div>


                        <div class="rows">
                                <div class="column">
                                        <div class="flip-container col1" ontouchstart="this.classList.toggle('hover');">
                                                <div class="flipper">
                                                    <div class="front">
                                                            <img  src="{{ URL::asset('front-theme/images/clients/img6.jpg')}}" width="100%" >
                                                    </div>
                                                    <div class="back" style="background-color:#15254c;">
                                                            <img  src="{{ URL::asset('front-theme/images/clients/proper-pizza.png')}}" width="40%" >
                                                    </div>
                                                </div>
                                            </div>
                                </div>
                                <div class="column">
                                    <a href="{{route('project2')}}">
                                        <div class="flip-container col2" ontouchstart="this.classList.toggle('hover');">
                                                <div class="flipper">
                                                    <div class="front">
                                                            <img  src="{{ URL::asset('front-theme/images/clients/img7.jpg')}}" width="100%" >
                                                    </div>
                                                    <div class="back" style="background-color:#978668;">
                                                        <img  src="{{ URL::asset('front-theme/images/clients/cocoon.png')}}" width="40%" >
                                                    </div>
                                                </div>
                                            </div>
                                    </a>
                                </div>
                                <div class="column">
                                        <div class="flip-container col3" ontouchstart="this.classList.toggle('hover');">
                                                <div class="flipper">
                                                    <div class="front">
                                                            <img  src="{{ URL::asset('front-theme/images/clients/img8.jpg')}}" width="100%" >
                                                    </div>
                                                    <div class="back" style="background-color:#537bb6;">
                                                        <img  src="{{ URL::asset('front-theme/images/clients/tui.png')}}" width="40%" >
                                                    </div>
                                                </div>
                                            </div>
                                </div>
                                <div class="column">
                                    <a href="http://www.experiencemarketing.co.nz/" target="_blank">
                                        <div class="flip-container col4" ontouchstart="this.classList.toggle('hover');">
                                            <div class="flipper">
                                                <div class="front">
                                                        <img  src="{{ URL::asset('front-theme/images/clients/img10.jpg')}}" width="100%" >
                                                </div>
                                                <div class="back" style="background-color:#000;">
                                                    <img  src="{{ URL::asset('front-theme/images/clients/experience.png')}}" width="70%" >
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>





                </div>
        </div>


        {{-- <div class="social">
                <a class="text">social links</a>
                <div class="line"></div>
                <a href="https://www.facebook.com/HuntCreativeLtd/?ref=bookmarks"><i class="fab fa-facebook" aria-hidden="true"></i>  </a>
                <a href="https://www.instagram.com/huntcreativeltd/"><i class="fab fa-instagram" aria-hidden="true"></i>  </a>
                <a href="https://www.linkedin.com/in/h%C3%A9l%C3%A8ne-hunt-42786645/"><i class="fab fa-linkedin" aria-hidden="true"></i>  </a>
                <a href="https://www.pinterest.ph/HuntCreativeLtd/"><i class="fab fa-pinterest" aria-hidden="true"></i>  </a>
            </div> --}}
</section>





<div id="MainNav">
        <a href="#MainNav" class="arrow"><i class="fa fa-chevron-down"></i></a>
        <div class="cont">
            {{-- <div class="row">
                    <div class="col-md-3 text-center wow animated fadeInLeft">
                        <img src="{{ URL::asset('front-theme/images/clients/offsure.png')}}" alt="Offsure" width="150px"/>
                    </div>
                    <div class="col-md-3 text-center wow animated fadeInLeft">
                        <img src="{{ URL::asset('front-theme/images/clients/cocoon.png')}}" alt="Cocoon" width="150px"/>
                    </div>
                    <div class="col-md-3 text-center wow animated fadeInRight">
                        <img src="{{ URL::asset('front-theme/images/clients/proper-pizza.png')}}" alt="Proper Pizza"  width="150px"/>
                    </div>
                    <div class="col-md-3 text-center wow animated fadeInRight">
                        <img src="{{ URL::asset('front-theme/images/clients/tui.png')}}" alt="Tui Moana" width="150px"/>
                    </div>
            </div> --}}
        </div>
</div>
<div class="row portfolio-mobile">

    <div class="owl-carousel work-areas text-center top_120 bottom_90 col-md-10 offset-md-1" data-pagination="true" data-autoplay="3000" data-items-desktop="3" data-items-desktop-small="3" data-items-tablet="2" data-items-tablet-small="1">
        <!-- an area -->
        <div class="area col-md-12 item">
                <img  src="{{ URL::asset('front-theme/images/clients/img1.jpg')}}" width="100%">
            <h3 class="title" class="title">Virtue Swim</h3>
            <div class="line"></div>
        </div>
        <!-- an area -->
        <div class="area col-md-12 item">
                <img  src="{{ URL::asset('front-theme/images/clients/img2.jpg')}}" width="100%">
            <h3 class="title">One Degree Consulting</h3>
            <div class="line"></div>
        </div>
        <!-- an area -->
        <div class="area col-md-12 item">
                <img  src="{{ URL::asset('front-theme/images/clients/img3.jpg')}}" width="100%">
            <h3 class="title">Waikato Valley Chocolates</h3>
            <div class="line"></div>
        </div>
        <!-- an area -->
        <div class="area col-md-12 item">
                <img  src="{{ URL::asset('front-theme/images/clients/img4.jpg')}}" width="100%">
            <h3 class="title">Construction Cost Consultants</h3>
            <div class="line"></div>
        </div>
        <!-- an area -->
        <div class="area col-md-12 item">
                <img  src="{{ URL::asset('front-theme/images/clients/img5a.jpg')}}" width="100%" >
            <h3 class="title">Offsure Ltd.</h3>
            <div class="line"></div>
        </div>
        <!-- an area -->
        <div class="area col-md-12 item">
                <img  src="{{ URL::asset('front-theme/images/clients/img6.jpg')}}" width="100%" >
            <h3 class="title">Proper Pizza</h3>
            <div class="line"></div>
        </div>
        <!-- an area -->
        <div class="area col-md-12 item">
                <img  src="{{ URL::asset('front-theme/images/clients/img7.jpg')}}" width="100%" >
            <h3 class="title">Cocoon House</h3>
            <div class="line"></div>
        </div>
        <!-- an area -->
        <div class="area col-md-12 item">
                <img  src="{{ URL::asset('front-theme/images/clients/img8.jpg')}}" width="100%" >
            <h3 class="title">Tui Moana</h3>
            <div class="line"></div>
        </div>
        <!-- an area -->
        <div class="area col-md-12 item">
                <img  src="{{ URL::asset('front-theme/images/clients/img9.jpg')}}" width="100%" >
            <h3 class="title">Warren Bike Tours</h3>
            <div class="line"></div>
        </div>

    </div>
</div>

<div class="cont main-about-margin">
    <section id="intro" id="MainNav">
        <div class="row mb-4">
            <div class="col-md-12 text-center" style="font-size:18px;">

                <p class="wow animated fadeInRight">We’re a New Zealand based creative agency providing branding, design, content creation and web development expertise. We help people and brands tell their story through meaningful design, strategic content and collaborative thinking.</p>

                <p class="wow animated fadeInRight">Our team’s individual talents combine to offer a comprehensive array of commercially focussed creativity consistently delivering an exceptionally high quality of work for our clients within budget and on time.</p>

                <p class="wow animated fadeInRight">We keep it simple, we make it beautiful.</p>
            </div>

        </div>


    </section>
    <br><br><br><br>
    <section id="expertise">
            <div class="row mb-4 wow animated fadeInDown" style="margin-bottom:20px;">
                <div class="col-md-12 text-center">
                        <h1>What We Do</h1>
                </div>

            </div>
            <div class="row ch-grid">
                <div class="col-md-4">
                    <li class="wow animated fadeInUp">
                        <div class="ch-item">
                            {{-- <div class="ch-info">
                                <div class="ch-info-front ch-img-1"></div>
                                <div class="ch-info-back serv1">

                                </div>
                            </div> --}}
                            <img src="{{ URL::asset('front-theme/images/service_graphic.png')}}" width="40%">
                        </div>
                        <div style="height: 100px;">
                            <h3 class="title mt-4 wow animated fadeInUp">Graphic Design</h3>
                            <div class="line wow animated fadeInUp"></div>
                            <p class="text-center wow animated fadeInUp">We work to understand you and your brand so we can ensure the journey is as rewarding as the destination.</p>

                        </div>
                    </li>
                </div>
                <div class="col-md-4">
                    <li class="wow animated fadeInDown">
                        <div class="ch-item">
                            {{-- <div class="ch-info">
                                <div class="ch-info-front ch-img-2"></div>
                                <div class="ch-info-back serv2">

                                </div>
                            </div> --}}
                            <img src="{{ URL::asset('front-theme/images/service_writing.png')}}" width="40%">
                        </div>
                        <h3 class="title mt-4 wow animated fadeInUp">Content Writing</h3>
                        <div class="line wow animated fadeInUp"></div>
                        <p class="text-center wow animated fadeInUp">We love telling stories, learning all the facts, stats and quotes about you and your business to ensure your story is told with a fresh perspective.</p>

                    </li>
                </div>
                <div class="col-md-4">
                    <li class="wow animated fadeInRight">
                        <div class="ch-item">
                            {{-- <div class="ch-info">
                                <div class="ch-info-front ch-img-3"></div>
                                <div class="ch-info-back serv3">

                                </div>
                            </div> --}}
                            <img src="{{ URL::asset('front-theme/images/service_web.png')}}" width="40%">
                        </div>
                        <h3 class="title mt-4 wow animated fadeInUp">Web Development</h3>
                        <div class="line wow animated fadeInUp"></div>
                        <p class="text-center wow animated fadeInUp">We have a full-stack developer on board to build, maintain, assess and service your online brand experience and engagement.</p>

                    </li>
                </div>
            </div>

    </section>

    <div class="row" style="margin-top:200px">
        <div class="col-md-12 text-center">
                <a href="{{ route('portfolio') }}" class="btn btn-white btn-animation-1 wow animated bounceInUp">View Our Projects</a>
        </div>
    </div>
    {{-- <div class="row mb-4 " style="padding-top:150px;">
        <div class="col-md-12 text-center">
                <h1>Our Works</h1>
        </div>

    </div> --}}

    {{-- <section id="portfolio" class="portfolio" >

            <div class="portfolio-filter row">
                <div data-filter=".digital" class="cbp-filter-item">Digital Works</div>
                <div data-filter=".photography" class="cbp-filter-item">Photography</div>
                <div data-filter=".branding" class="cbp-filter-item">Branding</div>
                <div data-filter="*" class="cbp-filter-item cbp-filter-item-active">All Works</div>
            </div>
            <div id="grid-container">

                <div class="cbp-item photography wow animated fadeInUp">
                    <a href="portfolio/work-1.html">
                        <figure class="fig">
                            <img src="{{ URL::asset('front-theme/images/work-1.jpg')}}" alt="">
                            <figcaption>
                                <h3>Cosplay</h3>
                                <p>Photography</p>
                            </figcaption>
                        </figure>
                    </a>
                </div>

                <div class="cbp-item digital wow animated fadeInUp">
                    <a href="portfolio/work-1.html">
                        <figure class="fig">
                            <img src="{{ URL::asset('front-theme/images/work-2.jpg')}}" alt="">
                            <figcaption>
                                <h3>Metra Park</h3>
                                <p>Branding </p>
                            </figcaption>
                        </figure>
                    </a>
                </div>

                <div class="cbp-item digital wow animated fadeInUp">
                    <a href="portfolio/work-1.html">
                        <figure class="fig">
                            <img src="{{ URL::asset('front-theme/images/work-3.jpg')}}" alt="">
                            <figcaption>
                                <h3>Socialmedia</h3>
                                <p>Digital </p>
                            </figcaption>
                        </figure>
                    </a>
                </div>

                <div class="cbp-item branding wow animated fadeInUp">
                    <a href="portfolio/work-1.html">
                        <figure class="fig">
                            <img src="{{ URL::asset('front-theme/images/work-4.jpg')}}" alt="">
                            <figcaption>
                                <h3>Vrai Vodka</h3>
                                <p>Branding </p>
                            </figcaption>
                        </figure>
                    </a>
                </div>

                <div class="cbp-item digital wow animated fadeInUp">
                    <a href="portfolio/work-1.html">
                        <figure class="fig">
                            <img src="{{ URL::asset('front-theme/images/work-5.jpg')}}" alt="">
                            <figcaption>
                                <h3>Smart Wallet</h3>
                                <p>Digital </p>
                            </figcaption>
                        </figure>
                    </a>
                </div>

                <div class="cbp-item branding wow animated fadeInUp">
                    <a href="portfolio/work-1.html">
                        <figure class="fig">
                            <img src="{{ URL::asset('front-theme/images/work-6.jpg')}}" alt="">
                            <figcaption>
                                <h3>Motorcycles</h3>
                                <p>Branding </p>
                            </figcaption>
                        </figure>
                    </a>
                </div>

                <div class="cbp-item branding wow animated fadeInUp">
                    <a href="portfolio/work-1.html">
                        <figure class="fig">
                            <img src="{{ URL::asset('front-theme/images/work-7.jpg')}}" alt="">
                            <figcaption>
                                <h3>Racquel Natasha</h3>
                                <p>Branding </p>
                            </figcaption>
                        </figure>
                    </a>
                </div>

                <div class="cbp-item branding wow animated fadeInUp">
                    <a href="portfolio/work-1.html">
                        <figure class="fig">
                            <img src="{{ URL::asset('front-theme/images/work-8.jpg')}}" alt="">
                            <figcaption>
                                <h3>Swacket</h3>
                                <p>Branding </p>
                            </figcaption>
                        </figure>
                    </a>
                </div>

                <div class="cbp-item branding wow animated fadeInUp">
                    <a href="portfolio/work-1.html">
                        <figure class="fig">
                            <img src="{{ URL::asset('front-theme/images/work-9.jpg')}}" alt="">
                            <figcaption>
                                <h3>The Gang</h3>
                                <p>Branding </p>
                            </figcaption>
                        </figure>
                    </a>
                </div>
            </div>

            <div id="port-loadMore" class="cbp-l-loadMore-button top_120 bottom_90">
                <a href="port.html" class="cbp-l-loadMore-link site-btn" rel="nofollow">
                    <span class="cbp-l-loadMore-defaultText">Load More (<span class="cbp-l-loadMore-loadItems">2</span>)</span>
                    <span class="cbp-l-loadMore-loadingText">Loading...</span>
                    <span class="cbp-l-loadMore-noMoreLoading">No More Works</span>
                </a>
            </div>
    </section> --}}

    {{-- <hr class="top_90 bottom_90 col-md-8">

    <section class="widget-twitter top_60">
         <div class="widget-title">
            <h2 class="classic-title">Latest Tweets</h2>
        </div>
        <div class="tweet"><ul><li class="item">12312</li></ul></div>
        <a href="https://twitter.com/envato" target="_blank" class="twitter-account">@envato</a>
    </section> --}}
</div> <!-- cont end -->
@endsection

@section('script')
    <script>
        $(".arrow").click(function() {
            $('html, body').animate({
                scrollTop: $("#MainNav").offset().top
            }, 1000);
        });
    </script>
@endsection



@extends('front.page-template')
@section('meta-title')
<title>We are hunt creative - Project|Offsure</title>
@endsection

@section('title')
    {{-- <section class="titlebar">
        <h1 class="page-title"><span>hunt </span>for creativity</h1>
        <div id="particles-js"></div>
    </section>

    <hr class="col-md-6 bottom_60"> --}}
@endsection

@section('content')
<div class="cont">
    <section class="top_per_20 bottom_per_20">
        <div class="row">
            <div class="col-md-6">
                    <h1 class="blue">Offsure Global</h1>
                    <h4>Building the Smile</h4>
                     <ul class="information">
                        <li><span>+</span>Client: Offsure</li>
                        <li><span>+</span>Website: <a href="https://offsure.com" target="_blank">www.offsure.com</a></li>
                        <li><span>+</span>Category: graphic design, content writing, web development</li>
                    </ul>
            </div>
        </div>

    </section>
    <section class="portfolio-single type-1 ">
        <figure class="hero-image wow animated fadeInUp">
            <img class="lazy" data-src="{{ URL::asset('front-theme/images/portfolio/work-1/offsure.jpg')}}" alt="">
        </figure>
        <div class="row top_60">
            <div class="col-md-6 wow animated fadeInLeft">
                    <h3 class="blue">Offsure Global</h3>
                    <p>Offsure is a leading provider of outsourcing solutions and services for businesses in different industries including real estate, accounting, ICT, customer support and more. Hunt Creative worked alongside Offsure to establish the company’s brand identity, marketing approach and positioning.</p>
            </div>
            <div class="col-md-6 wow animated fadeInRight">
                    <h5 class="blue">THE CHALLENGE</h5>
                    <p>It was a big challenge starting from scratch and integrating the Offsure brand in different online platforms and marketing campaigns to convert leads into clients.</p>
                    <p>Another struggle was to develop and design a website that will serve as a recruitment and marketing hub where in jobseekers can apply for job opportunities and businesses to source professionals they’re looking for.</p>
                    {{-- <h5 class="blue">THE OUTCOME</h5>
                    <p>Three engaging apps, designed and built by BKA in 4 weeks for New Zealand’s only boat show on the water.</p> --}}
            </div>
        </div>
        <div class="row top_60">
            <figure class="hero-image wow animated fadeInUp">
                <img class="lazy" data-src="{{ URL::asset('front-theme/images/portfolio/work-1/hunt_macbook.png')}}" alt="">
            </figure>
            <div class="col-md-6 wow animated fadeInLeft">

                    <p>OFFSURE is ready to source and recruit the best employees to help you where your business needs it. We want to ensure this process is as painless as possible for you. We’ll source, review and test applicants as required - you interview our top picks and the choice is up to you. Monthly desk cost per employee ranges from $1,200 to $2,250 based on the role.</p>
            </div>
            <div class="col-md-6 wow animated fadeInRight">

                    <p>You can be part of OFFSURE too - client or employee. As a client, we’ll help you source the best talent available and ensure they hit the ground sprinting. For job seekers, OFFSURE is here to provide the stage for you, show them what you got. </p>


            </div>
        </div>
        <div class="row top_150">
                <figure class="hero-image wow animated fadeInUp">
                    <img class="lazy" data-src="{{ URL::asset('front-theme/images/portfolio/work-1/offsure_phone.png')}}" alt="">
                </figure>

            </div>
        {{-- <ul class="information">
            <li><span>Client:</span> Themeforest</li>
            <li><span>Date:</span> 8 March 2018</li>
            <li><span>Website:</span> dribbble.com</li>
            <li><span>Category:</span> art, retouch, photography</li>
        </ul>
        <h1 class="title bottom_15">What about if he reported sick? But that would be extremely strained and suspicious as in fifteen years of service Gregor had never once yet been ill.</h1>
        <p>For the photographers, one of the city's most intriguing traits is the pastel pink coloring of its buildings. "The first gates you see when you enter are pink," said Wong. "Once you're through, everything around you varies in different shades of the color -- from bright pinks to reddish browns." One pink palace proved especially popular on social media.</p> --}}

        <div class="portfolio-lightbox top_60 row">
            <figure class="col-md-6 bottom_30 wow animated fadeInLeft">
                <img class="lazy" data-src="{{ URL::asset('front-theme/images/portfolio/work-1/port1.jpg')}}" alt="">
            </figure>
            <figure class="col-md-6 bottom_30 lightbox wow animated fadeInRight">
                <img class="lazy" data-src="{{ URL::asset('front-theme/images/portfolio/work-1/port5.jpg')}}" alt="">
            </figure>
            <figure class="col-md-3 wow animated fadeInLeft">
                <img class="lazy" data-src="{{ URL::asset('front-theme/images/portfolio/work-1/port2.jpg')}}" alt="">
            </figure>
            <figure class="col-md-3 wow animated fadeInRight">
                <img class="lazy" data-src="{{ URL::asset('front-theme/images/portfolio/work-1/port3.jpg')}}" alt="">
            </figure>
            <figure class="col-md-3 wow animated fadeInLeft">
                <img class="lazy" data-src="{{ URL::asset('front-theme/images/portfolio/work-1/port4.jpg')}}" alt="">
            </figure>
        </div>

        <div class="col-md-12 portfolio-nav text-center top_90">
            <a class="port-next" href="{{route('project2')}}">
                <div class="nav-title">next</div>
                <div class="next-title">Cocoon House</div>
            </a>
        </div>

    </section>

</div> <!-- cont end -->
@endsection

@extends('front.page-template')
@section('meta-title')
<title>We are hunt creative - Project|Cocoon House</title>
@endsection

@section('title')
    {{-- <section class="titlebar">
        <h1 class="page-title"><span>hunt </span>for creativity</h1>
        <div id="particles-js"></div>
    </section>

    <hr class="col-md-6 bottom_60"> --}}
@endsection

@section('content')
<div class="cont">
    <section class="top_per_20 bottom_per_20">
        <div class="row">
            <div class="col-md-6">
                    <h1 class="blue">Cocoon House</h1>
                    <h4>Home to the Cocoon Salon of Art, Wine & Design</h4>
                     <ul class="information">
                        <li><span>+</span>Client: Cocoon</li>
                        <li><span>+</span>Website: <a href="https://cocoonhouse.co.nz" target="_blank">www.cocoonhouse.co.nz</a></li>
                        <li><span>+</span>Category: graphic design, content writing, web development</li>
                    </ul>
            </div>
        </div>

    </section>
    <section class="portfolio-single type-1 ">
        <figure class="hero-image wow animated fadeInUp">
            <img class="lazy" data-src="{{ URL::asset('front-theme/images/portfolio/work-2/intro.jpg')}}" alt="">
        </figure>
        <div class="row top_60">
            <div class="col-md-6 wow animated fadeInLeft">
                    <h3 class="blue">Cocoon House</h3>
                    <p>Auckland’s number 1 private events space. Cocoon House offers nothing but Kiwi hospitality at its finest, international luxury, curated rooms filled with handcrafted furniture, stunning interior design, renowned art pieces, and last but not the lease, home to the historic Jiangan Tea Ceremony from ancient China.
                    <br><br>Cocoon House caters to all sorts of events from cocktail parties all the way to exclusive business meetings. All spaces are designed by the best selection of decorations inspired and produced by some of the greatest artists from different generations.</p>
            </div>
            <div class="col-md-6 wow animated fadeInRight">
                    <h5 class="blue">THE CHALLENGE</h5>
                    <p>We worked alongside Rosa Tan, owner of Cocoon House in building the brand from scratch. We started marketing through Instagram by posting photos of the curated space to give the target audience a glimpse of the house.</p>
                    <p>It was a big challenge to gain engagement online especially to start on a social media platform with no existing website nor any marketing campaigns. However, with Tan's connection, colleagues and friends, the account got quite the followers and a lot of engagement on the first couple of weeks.</p>
                    <p>The next thing to do now is to integrate Cocoon House online and to come up a consistent marketing strategy to attract new members and retain the regular ones.</p>
                    {{-- <h5 class="blue">THE OUTCOME</h5>
                    <p>Three engaging apps, designed and built by BKA in 4 weeks for New Zealand’s only boat show on the water.</p> --}}
            </div>
        </div>
        <div class="row top_60">
            <figure class="hero-image wow animated fadeInUp">
                <img class="lazy" data-src="{{ URL::asset('front-theme/images/portfolio/work-2/cocoon1.png')}}" alt="">
            </figure>
            {{-- <div class="col-md-6 wow animated fadeInLeft">

                    <p>OFFSURE is ready to source and recruit the best employees to help you where your business needs it. We want to ensure this process is as painless as possible for you. We’ll source, review and test applicants as required - you interview our top picks and the choice is up to you. Monthly desk cost per employee ranges from $1,200 to $2,250 based on the role.</p>
            </div>
            <div class="col-md-6 wow animated fadeInRight">

                    <p>You can be part of OFFSURE too - client or employee. As a client, we’ll help you source the best talent available and ensure they hit the ground sprinting. For job seekers, OFFSURE is here to provide the stage for you, show them what you got. </p>


            </div> --}}
        </div>
        <div class="row top_150">
                <figure class="hero-image wow animated fadeInUp">
                    <img class="lazy" data-src="{{ URL::asset('front-theme/images/portfolio/work-2/cocoon2.png')}}" alt="">
                </figure>

            </div>
        {{-- <ul class="information">
            <li><span>Client:</span> Themeforest</li>
            <li><span>Date:</span> 8 March 2018</li>
            <li><span>Website:</span> dribbble.com</li>
            <li><span>Category:</span> art, retouch, photography</li>
        </ul>
        <h1 class="title bottom_15">What about if he reported sick? But that would be extremely strained and suspicious as in fifteen years of service Gregor had never once yet been ill.</h1>
        <p>For the photographers, one of the city's most intriguing traits is the pastel pink coloring of its buildings. "The first gates you see when you enter are pink," said Wong. "Once you're through, everything around you varies in different shades of the color -- from bright pinks to reddish browns." One pink palace proved especially popular on social media.</p> --}}

        <div class="portfolio-lightbox top_60 row">
            <figure class="col-md-6 bottom_30 wow animated fadeInLeft">
                <img class="lazy" data-src="{{ URL::asset('front-theme/images/portfolio/work-2/cocoon3.png')}}" alt="">
            </figure>
            <figure class="col-md-6 bottom_30 lightbox wow animated fadeInRight">
                <img class="lazy" data-src="{{ URL::asset('front-theme/images/portfolio/work-2/tea-menu.jpg')}}" alt="">
            </figure>
            <figure class="col-md-3 wow animated fadeInLeft">
                <img class="lazy" data-src="{{ URL::asset('front-theme/images/portfolio/work-2/tea-menu1.jpg')}}" alt="">
            </figure>
            <figure class="col-md-3 wow animated fadeInRight">
                <img class="lazy" data-src="{{ URL::asset('front-theme/images/portfolio/work-2/tea-menu2.jpg')}}" alt="">
            </figure>
            <figure class="col-md-3 wow animated fadeInLeft">
                <img class="lazy" data-src="{{ URL::asset('front-theme/images/portfolio/work-2/cocoon-wine-bottle.jpg')}}" alt="">
            </figure>
        </div>

        <div class="col-md-12 portfolio-nav text-center top_90">
            <a class="port-next" href="work-2.html">
                <div class="nav-title">next</div>
                <div class="next-title">Metra Park</div>
            </a>
        </div>

    </section>

</div> <!-- cont end -->
@endsection

@extends('front.page-template')
@section('meta-title')
<title>We are hunt creative - Services</title>
@endsection

@section('title')
    <section class="titlebar">
        <h1 class="page-title">we are <span>awesome</span> at</h1>
        {{-- <div id="particles-js"></div> --}}
    </section>

    <hr class="col-md-6 bottom_60">
@endsection

@section('content')
<div class="cont">
    <div class="row mb-4 wow animated fadeInDown" style="padding-bottom:150px;">
        <div class="col-md-12 text-center">
                <h1>It does not matter if your branding is starting from scratch or 99% done. We will accommodate all your creative and branding requirements. </h1>
        </div>

    </div>

    <section id="graphic-design" class="services">
        <div class="row">
            <div class="col-md-3">
                <h2 class="title wow animated fadeInLeft">Graphic <span>Design</span></h2>
                <blockquote class="wow animated bounceInLeft">
                <p><q>Make it simple, but significant</q></p>
                <div class="footer">Don Draper</div>
                </blockquote>
            </div>
            <div class="col-md-9" >
                <div class="details wow animated bounceInLeft">
                        <p>We understand the brand journey is one to be enjoyed - as the destination, otherwise known as the finish line, really doesn't exist. There are some fantastic layovers in some amazing places but we can't stay there forever. Your brand on paper, online, in minds and hearts has to keep moving and growing, there are so many more places to see and experience.</p>
                        <p>We work to understand you and your brand so we can ensure the journey is as rewarding as the destination.</p>
                    </div>
                <div class="row" style="margin-top:40px;">
                    <div class="col-md-6">
                        <div class="services_list__wrap">
                            <ul class="services_list">
                                <li class="services_list_line wow animated fadeInUp"><span>+</span>Logo development</li>
                                <li class="services_list_line wow animated fadeInUp"><span>+</span>Brand & ffice collateral design</li>
                                <li class="services_list_line wow animated fadeInUp"><span>+</span>Brand & Corporate Identity</li>
                                <li class="services_list_line wow animated fadeInUp"><span>+</span>Image enhancement & revitalization</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="services_list__wrap">
                            <ul class="services_list">
                                <li class="services_list_line wow animated fadeInUp"><span>+</span>Promotional posters, flyers & banners</li>
                                <li class="services_list_line wow animated fadeInUp"><span>+</span>Vector Illustration</li>
                                <li class="services_list_line wow animated fadeInUp"><span>+</span>UX Design</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 details">
                        <a href="{{ route('contact') }}" class="btn btn-white btn-animation-1 wow animated bounceInUp">Start a Project</a>
                    </div>
                </div>
                {{-- <div class="ser_img">
                    <img src="{{ URL::asset('front-theme/images/service_graphic.png')}}"/>
                </div> --}}
            </div>

        </div>
    </section>
    <section id="content-writing" class="services">
        <div class="row">
            <div class="col-md-3 ">
                <h2 class="title wow animated fadeInLeft">Content <span>Writing</span></h2>
                <blockquote class="wow animated bounceInLeft">
                    <p><q>I know words. I have the best words.</q></p>
                    <div class="footer">Donald Trump</div>
                </blockquote>
            </div>
            <div class="col-md-9" >
                <div class="details wow animated fadeInRight">
                    <p>So what does storytelling have to do with your business - only everything. Merging stories into your business communication will pique interest within your existing audience and captivate new subscribers, passively educating - turning readers into leads, leads into customers and your customers into lifers.</p>
                    <p>We love telling stories, learning all the facts, stats and quotes about you and your business to ensure your story is told with a fresh perspective.</p>
                </div>
                <div class="row" style="margin-top:40px;">
                    <div class="col-md-6">
                        <div class="services_list__wrap">
                            <ul class="services_list">
                                <li class="services_list_line wow animated fadeInUp"><span>+</span>Mission, vision & values</li>
                                <li class="services_list_line wow animated fadeInUp"><span>+</span>Website write-up</li>
                                <li class="services_list_line wow animated fadeInUp"><span>+</span>Taglines & one-liners</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="services_list__wrap">
                            <ul class="services_list">
                                <li class="services_list_line wow animated fadeInUp"><span>+</span>Tone of voice & messaging</li>
                                <li class="services_list_line wow animated fadeInUp"><span>+</span>Brand story</li>
                                <li class="services_list_line wow animated fadeInUp"><span>+</span>Blogging</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 details">
                            <a href="{{ route('contact') }}" class="btn btn-white btn-animation-1 wow animated bounceInUp">Start a Project</a>
                        </div>
                </div>
                {{-- <div class="ser_img">
                    <img src="{{ URL::asset('front-theme/images/service_writing.png')}}"/>
                </div> --}}
            </div>

        </div>
    </section>

    <section id="web-development" class="services" style="margin-bottom: 100px;">
        <div class="row">
            <div class="col-md-3">
                <h2 class="title wow animated fadeInLeft">Web <span>Dev</span></h2>
                <blockquote class="wow animated bounceInLeft">
                    <p><q>Design is not just what it looks like and feels like. Design is how it works.</q></p>
                    <div class="footer">Steve Jobs</div>
                </blockquote>
            </div>
            <div class="col-md-9" >
                <div class="details wow animated fadeInRight">
                    <p>A website can't just have a pretty face - it must be efficient, strategic, goal-oriented, user-driven and innovative… that is what makes a web portal a BEAUTY and a BEAST at the same time. We offer senior web development and maintenance services. No need to worry about the front and the back and the middle.</p>
                    <p>We have a full-stack developer on board to build, maintain, assess and service your online brand experience and engagement.</p>
                </div>
                <div class="row" style="margin-top:40px;">
                    <div class="col-md-7">
                        <div class="services_list__wrap">
                            <ul class="services_list">
                                <li class="services_list_line wow animated fadeInUp"><span>+</span>Research & concept</li>
                                <li class="services_list_line wow animated fadeInUp"><span>+</span>Website generation</li>
                                <li class="services_list_line wow animated fadeInUp"><span>+</span>Responsive & strategic design</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="services_list__wrap">
                            <ul class="services_list">
                                <li class="services_list_line wow animated fadeInUp"><span>+</span>Form integration</li>
                                <li class="services_list_line wow animated fadeInUp"><span>+</span>Social media integration</li>
                                <li class="services_list_line wow animated fadeInUp"><span>+</span>Customisation</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-6 details">
                            <a href="{{ route('contact') }}" class="btn btn-white btn-animation-1 wow animated bounceInUp">Start a Project</a>
                        </div>
                </div>
                {{-- <div class="ser_img">
                    <img src="{{ URL::asset('front-theme/images/service_web.png')}}"/>
                </div> --}}
            </div>

        </div>
    </section>



</div> <!-- cont end -->
@endsection

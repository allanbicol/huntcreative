<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;

class AdminController extends Controller
{
    function login(){
        return view('back.Auth.login');
    }

    function checklogin(Request $request)
    {
        $this->validate($request,[
            'email'     =>  'required|email',
            'password'  =>  'required|alphaNum|min:3'
        ]);

        $user_data = array(
            'email'     =>  $request->get('email'),
            'password'  =>  $request->get('password')
        );

        if(Auth::attempt($user_data))
        {
            return redirect('/admin/dashboard');
        }else{
            return back()->with('error','Wrong Login Details');
        }
    }


    function logout(){
        Auth::logout();
        return redirect('/hc-admin');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendMail;
use Mail;
use Socketlabs\SocketLabsClient;
use Socketlabs\Message\BasicMessage;
use Socketlabs\Message\EmailAddress;

class PageController extends Controller
{
    function index()
    {
        return view('front.pages.index');
    }

    function aboutus()
    {
        return view('front.pages.aboutus');
    }

    function portfolio()
    {
        return view('front.pages.portfolio');
    }

    function services()
    {
        return view('front.pages.services');
    }

    function contact()
    {
        return view('front.pages.contact');
    }

    function newproject(){
        return view('front.pages.new-project');
    }

    function sendEmail(Request $request)
    {

        $data = [
            'name'=>$request->input('name'),
            'company'=>$request->input('company'),
            'email'=>$request->input('email'),
            'phone'=>$request->input('phone'),
            'services'=>$request->input('service'),
            'message'=>$request->input('message'),
        ];
        // dump($data);
        // print_r($data['services']);
        //return view('front.mail.contact',['data'=>$data]);
        // exit();

       // Mail::to('bicolallan@gmail.com')->send(new SendMail($data));

        // $email = Mail::send('front.mail.contact', ['data'=>$data], function($message) {
        //     $message->to('allan.bicol@offsure.com');
        //     $message->from('helene@huntcreative.co.nz',"hunt creative ltd.");
        //     $message->subject('hunt creative inquiry');
        // });

        $serverId = env('socketLab_server_id');
        $injectionApiKey = env('socketLab_injection_key');

        $client = new SocketLabsClient($serverId, $injectionApiKey);

        $message = new BasicMessage();

        $message->subject = "hunt creative inquiry notification";
        $message->plainTextBody = "This is the Plain Text Body of my message.";
        $message->htmlBody = view('front.mail.contact',['data'=>$data])->render();

        $message->from = new EmailAddress("helene@huntcreative.co.nz","hunt creative ltd.");
        $message->replyTo = new EmailAddress("helene@huntcreative.co.nz");
        $message->addToAddress("helene@huntcreative.co.nz");
        // $message->replyTo = new EmailAddress("bicolallan@gmail.com");
        // $message->addToAddress("bicolallan@gmail.com");

        $response = $client->send($message);



        return back()->with('success','Message sent successfully!');
    }
}

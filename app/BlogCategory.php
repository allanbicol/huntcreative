<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'blog_category';

    protected $fillable = ['name','slug','created_at'];
    protected $guarded = ['description'];

    public $timestamps = false;
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@index')->name('homepage');

Route::get('/aboutus', 'PageController@aboutus')->name('aboutus');
Route::get('/portfolio', 'PageController@portfolio')->name('portfolio');
Route::get('/services', 'PageController@services')->name('services');
Route::get('/contact', 'PageController@contact')->name('contact');
Route::get('/new-project', 'PageController@newproject')->name('new-project');
Route::post('/send-mail', 'PageController@sendEmail')->name('send-mail');

//PORTFOLIO PAGES
Route::get('/project/offsure', 'FrontPortfolioController@project1')->name('project1');
Route::get('/project/cocoon-house', 'FrontPortfolioController@project2')->name('project2');



//ADMIN ROUTINGS
Route::get('/hc-admin', 'AdminController@login')->name('admin-login');
Route::post('/auth/checklogin','AdminController@checklogin')->name('checklogin');
Route::get('/auth/logout','AdminController@logout')->name('logout');
Route::get('/admin/dashboard', 'DashboardController@index')->name('dashboard');

//Projects
Route::get('/admin/projects','ProjectsController@index')->name('projects');
Route::get('/admin/projects-add','ProjectsController@add')->name('projects-add');
Route::post('/admin/projects-add-action','ProjectsController@addAction')->name('projects-add-action');
Route::get('/admin/projects-category','ProjectsController@category')->name('projects-category');
Route::post('/admin/projects-add-category','ProjectsController@addCategory')->name('projects-add-category');

//Blogs
Route::get('/admin/blog-category','BlogsController@category')->name('blog-category');
Route::post('/admin/add-blog-category','BlogsController@addCategory')->name('add-blog-category');
